import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ContainerMap {

    private static ContainerMap containerMap = null;
    private volatile Map<String, Integer> container;

    private ContainerMap() {
        container = new HashMap<>();
    }

    public static ContainerMap getInstance() {
        if (containerMap == null) {
            containerMap = new ContainerMap();
        }
        return containerMap;
    }

    public void addCurrency(Currency currency) {
        String cur = currency.getCurrency();
        Integer amount = currency.getAmount();
        if (container.containsKey(cur)) {
            int oldAmount = container.get(cur);
            if (oldAmount + amount != 0){
                container.put(cur, oldAmount + amount);
            } else {
                container.remove(cur);
            }
        } else {
            if (amount != 0){
                container.put(cur, amount);
            }
        }
    }

    public Integer getCurrencyAmount(String currency) {
        return container.get(currency);
    }

    public Set<String> getCurrencyNames() {
        return container.keySet();
    }

}
