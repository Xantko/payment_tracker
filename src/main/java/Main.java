import org.apache.commons.cli.*;
import org.apache.log4j.Logger;


import java.io.*;
import java.util.Scanner;

public class Main {

    private static final Logger LOGGER = Logger.getLogger(Main.class);

    public static void main(String[] args) {
        ContainerMap container = ContainerMap.getInstance();
        String inputFilePath = getInputFilepath(args);
        if (inputFilePath != null) {
            addCurrencyFromFile(inputFilePath);
        }
        OutThread outThread = new OutThread();
        outThread.start();
        while (true) {
            Scanner scannerCL = new Scanner(System.in);
            try {
                String inputStr = scannerCL.nextLine();
                if (inputStr.equalsIgnoreCase("exit")) {
                    System.exit(0);
                }
                container.addCurrency(parseCurrency(inputStr));
            } catch (IllegalValueException e) {
                System.out.println(e.getMessage());
                LOGGER.warn(e.getMessage());
            }
        }
    }

    public static String getInputFilepath(String[] args) {
        Options options = new Options();
        Option input = new Option("i", "input", true, "input file path");
        input.setRequired(false);
        options.addOption(input);

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = null;
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            LOGGER.warn(e.getMessage());
        }
        if (cmd != null) {
            return cmd.getOptionValue("input");
        }
        return null;
    }

    public static Currency parseCurrency(String line) throws IllegalValueException {
        String[] splitStr = line.trim().split("\\s+");
        if (splitStr.length < 2) {
            throw new IllegalValueException("Wrong value!");
        }
        Integer amount;
        try{
            amount = Integer.parseInt(splitStr[1]);
        } catch (NumberFormatException e){
            throw new IllegalValueException("Wrong amount!");
        }
        return new Currency(splitStr[0], amount);
    }

    private static void addCurrencyFromFile(String filePath) {
        try (FileInputStream stream = new FileInputStream(filePath)) {
            Scanner scan = new Scanner(stream);
            int lineNumber = 0;
            try {
                while (scan.hasNextLine()) {
                    try {
                        lineNumber++;
                        ContainerMap.getInstance().addCurrency(parseCurrency(scan.nextLine()));
                    } catch (IllegalValueException e) {
                        LOGGER.warn("Wrong value in " + lineNumber + " line.");
                    }
                }
            } catch (NumberFormatException e) {
                LOGGER.warn("Wrong value in " + lineNumber + " line. Error message: " + e.getMessage());
            }
        } catch (IOException e) {
            System.out.println("File not found!");
            LOGGER.warn(e.getMessage());
        }
    }

}
