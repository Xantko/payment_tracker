import com.google.gson.*;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Set;

public class OutThread extends Thread {

    private final Logger LOGGER = Logger.getLogger(OutThread.class);

    private JsonObject jsonCurrency = null;
    private static final String URL_STR = "https://v3.exchangerate-api.com/bulk/a5ace2b58004e165d9d2ad8b/USD";

    public void run() {
        Double amountUSD;
        String resultAmount;
        while (true) {
            Set<String> currencyNames = ContainerMap.getInstance().getCurrencyNames();
            for (String currency : currencyNames) {
                Integer amount =  ContainerMap.getInstance().getCurrencyAmount(currency);
                if (currency.equals("USD")) {
                    System.out.println(currency + " " + amount);
                } else {
                    amountUSD = getUSDAmount(currency, amount);
                    if (amountUSD != null) {
                        resultAmount = " (USD " + String.format("%.2f", amountUSD) + ")";
                    } else {
                        resultAmount = " (Unknown rate)";
                    }
                    System.out.println(currency + " " + amount + resultAmount);
                }
            }
            try {
                Thread.sleep(60000);
            } catch (InterruptedException e) {
                LOGGER.warn(e.getMessage());
            }
        }
    }

    private Double getUSDAmount(String currency, Integer amount) {

        if (jsonCurrency == null) {
            jsonCurrency = getJson();
        }
        if (jsonCurrency != null) {
            try {
                return amount / jsonCurrency.get(currency).getAsDouble();
            } catch (NullPointerException e) {
                return null;
            } catch (ArithmeticException e) {
                LOGGER.warn(e.getMessage());
                return null;
            }
        }
        return null;
    }

    public JsonObject getJson() {
        try {
            URL url = new URL(URL_STR);
            HttpURLConnection request = (HttpURLConnection) url.openConnection();
            request.connect();
            JsonParser jp = new JsonParser();
            JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
            JsonObject jsonObj = root.getAsJsonObject();
            String req_result = jsonObj.get("result").getAsString();
            if (req_result.equals("success")) {
                return jsonObj.get("rates").getAsJsonObject();
            } else {
                System.out.println("Error while getting rates data!");
            }
        } catch (IOException | JsonIOException | JsonSyntaxException e) {
            LOGGER.warn(e.getMessage());
        }
        return null;
    }
}
