public class Currency {
    private final String currency;
    private Integer amount;

    public Currency(String currency, Integer amount) {
        this.currency = currency;
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public Integer getAmount() {
        return amount;
    }
}
