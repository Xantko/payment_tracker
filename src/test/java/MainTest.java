import org.junit.Test;

import static org.junit.Assert.*;

public class MainTest {

    @Test
    public void testParseCurrency(){
        Currency currency;
        try {
            currency = Main.parseCurrency("CZK 500");
            assertEquals("CZK", currency.getCurrency());
            assertEquals(500, currency.getAmount().intValue());

            assertThrows(IllegalValueException.class, () -> Main.parseCurrency("CZK500"));
            assertThrows(IllegalValueException.class, () -> Main.parseCurrency("USD 44L"));
            assertThrows(IllegalValueException.class, () -> Main.parseCurrency(""));
        } catch (IllegalValueException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetInputFilePath(){
        assertEquals("C:/text.txt", Main.getInputFilepath(new String[]{"-i", "C:/text.txt"}));
        assertEquals("C:/text.txt", Main.getInputFilepath(new String[]{"--input", "C:/text.txt"}));
        assertNull(Main.getInputFilepath(new String[]{"-abc132"}));
        assertNull(Main.getInputFilepath(new String[0]));
    }
}
