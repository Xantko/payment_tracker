import org.junit.Test;

import static org.junit.Assert.*;

public class ContainerMapTest {

    @Test
    public void testGetContainer(){
        ContainerMap container = ContainerMap.getInstance();

        assertNull(container.getCurrencyAmount("USD"));

        container.addCurrency(new Currency("USD", 0));
        assertNull(container.getCurrencyAmount("USD"));

        container.addCurrency(new Currency("USD", 100));
        assertEquals(100, container.getCurrencyAmount("USD").intValue());

        container.addCurrency(new Currency("USD", 0));
        assertEquals(100, container.getCurrencyAmount("USD").intValue());

        container.addCurrency(new Currency("USD", -100));
        assertNull(container.getCurrencyAmount("USD"));

        container.addCurrency(new Currency("USD", -50));
        assertEquals(-50,container.getCurrencyAmount("USD").intValue());
    }
}
