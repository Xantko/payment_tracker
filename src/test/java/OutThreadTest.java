import org.junit.Test;

import static org.junit.Assert.*;


public class OutThreadTest {

    @Test
    public void testGetJsonInfo(){
        OutThread outThread = new OutThread();

        assertNotNull(outThread.getJson());
        assertTrue(outThread.getJson().has("USD"));
        assertTrue(outThread.getJson().has("EUR"));
        assertTrue(outThread.getJson().has("CZK"));
        assertTrue(outThread.getJson().has("RUB"));
        assertFalse(outThread.getJson().has("AAA"));
        assertEquals(outThread.getJson().get("USD").getAsInt(), 1);
    }
}
