# Payment tracker
The application can be started with parameters as `-i "file_path"` or `--input "file_path"` and without parameters.

If you start the application with parameter it will read information from the file, write out result in console
and wait for inputting new record in console.

This project contains built jar file in a `jar_file` folder.
### Examples
Start file without parameters example: 

    `java -jar payment-tracker-1.0-SNAPSHOT-jar-with-dependencies.jar`
    
Start file with parameters example:
 
    `java -jar payment-tracker-1.0-SNAPSHOT-jar-with-dependencies.jar -i C:\file.txt`